from selenium import webdriver
import unittest
import time


class cevaTest(unittest.TestCase):
    def setUp(self):
        self.profile = webdriver.FirefoxProfile()
        self.profile.accept_untrusted_certs = True
        self.profile.accept_ssl_certs = True
        self.driver = webdriver.Firefox(firefox_profile=self.profile)
        self.driver.set_window_size(height=1920, width=1080)
        self.prenume = "Dan Capitan"
        self.nume = "Bucatariu"
        self.telefon = "0745 831 673"
        self.email = "dancap.buca@gmail.com"
        self.parola = "Paroladetest1!"

    def testCreareCont(self):
        self.driver.get("https://unsite.ro")
        if "https://www.unsite.ro/" == self.driver.current_url:
            print("Site-ul este bun")
        else:
            raise Exception("Site-ul nu  este bun")
        time.sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[1]/header/div[1]/nav/a").click()
        time.sleep(2)
        if "https://www.unsite.ro/autentificare/" == self.driver.current_url:
            print("Am ajuns la pagina de register/login")
        else:
            raise Exception("Pagina nu este corecta")

        prenume = self.driver.find_element_by_xpath('//*[@id="newfirstname"]')
        prenume.send_keys(self.prenume)

        nume = self.driver.find_element_by_xpath('//*[@id="newlastname"]')
        nume.send_keys(self.nume)

        telefon = self.driver.find_element_by_xpath('//*[@id="telephone"]')
        telefon.send_keys(self.telefon)

        email = self.driver.find_element_by_xpath('//*[@id="newemail"]')
        email.send_keys(self.email)

        parola = self.driver.find_element_by_xpath('//*[@id="newpassword"]')
        parola.send_keys(self.parola)

        parola2 = self.driver.find_element_by_xpath('//*[@id="newpasswordretype"]')
        parola2.send_keys("222")
        self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/div[2]/form/div/div/button').click()
        time.sleep(5)
        if "Parolele nu corespund" in self.driver.page_source:
            print("Parolele nu corespund")
        else:
            raise Exception("ceva s-a intamplat la creare cont")

    def tearDown(self) -> None:
        print("Inchidem Testul.")
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
